# Save Webpage To EazyPaper

Save Webpage To EazyPaper is a chromium addon to save webpages to your Downloads folder for offline archiving and browsing. Use one click to save a webpage offline, and another click to cite it in Microsoft Word with the optional EazyPaper addon (installed separately at https://eazypaper.com/download).

## Design Goals
Save Webpage to EazyPaper is designed to be as simple and bullet-proof for you as possible. This means minimizing the number of:
* Configuration options (Currently, there are none)
* Clicks in the user interface (Only a single click is needed to save the current webpage to the Downloads\EazyPaper folder. Even the popup window disappears after 2 seconds with no user interaction.)
* Dependencies external to Chrome (Currently, there are none)

We also designed the addon to be as simple as possible for developers to understand and reuse the code. This means:
* Minimizing the number of permissions in the manifest.json file
* Writing the addon in pure Javascript, CSS, and HTML; thus, there are no build tools or environment variables to set up
* Avoiding the use of native messaging to the EazyPaper Word addon

## How Save Webpage to EazyPaper interacts with EazyPaper
Save Webpage to EazyPaper saves the current tab's webpage in MHTML format to the Downloads\EazyPaper folder in a cross-platform way. It has no dependencies to EazyPaper.exe by design and does not require EazyPaper to be installed to run. This means that regardless of whether you install EazyPaper.exe or not, you can still use the core feature of saving a webpage for offline access for free, forever.

To cite those saved webpages though, you need to have EazyPaper installed. EazyPaper is Microsoft Word addon that runs only in Windows. It installs the Save Webpage to EazyPaper extension into chromium browsers upon its installation, or the user can install the extension from the Chrome web store. If the latter, then the user has to download and install EazyPaper at https://eazypaper.com/download to acquire the optional citation features.

Since EazyPaper is a full Win32 program, it can read the Downloads\EazyPaper folder, parse the .mht files and extract the bibliographic information from them. You can then insert the resulting AMA, APA, MLA, or Turabian / Chicago references into your Microsoft Word document.

EazyPaper does not care where the .mht files come from, so you save your own .mht files and place them in the Downloads\EazyPaper folder to have EazyPaper import them. Similarly, you can delete .mht files from the Downloads\EazyPaper folder to remove them from EazyPaper; however, if you insert an imported reference into your paper, it will remain in your paper's database of references regardless of whether you deleted the .mht file from the Downloads\EazyPaper folder.

## License
Save Webpage to EazyPaper is licensed under an Apache 2.0 license. This basically means that you're free to reuse and modify this code for your projects as long as your replace the name "EazyPaper" and the EazyPaper icons with your own versions. You can reuse our code, but not our identity.

EazyPaper is a closed source, commercial Microsoft Word addon that is not distributed with this chromium extension. You can view the EazyPaper demo at https://eazypaper.com/demo and download a 2-day free trial at https://eazypaper.com/download

## Contributing
Contributions to the Save Webpage to EazyPaper chromium extension are welcome. Since this extension was developed by a commercial company rather than just in someone's free time, responses to reported GitLab issues should be fairly quick. To set up a development environment:

1. Clone this repository into a local folder on your computer
2. Go to Chrome->Puzzle icon (top right hand corner in Chrome)->Manage extensions
3. Turn on developer mode
4. Click "Load unpacked" and browse to the folder you created in step 1

This extension should now be loaded in Chrome and any changes to the source files are live as soon as you save them. The only exception is the service worker, sw.js. You have to go to Chrome->Puzzle icon->Manage extensions->Save Webpage to EazyPaper->reload icon to reload the sw.js file.

Issues related to **saving** a webpage should be reported to: https://gitlab.com/eazypaper/save-webpage-to-eazypaper/-/issues

Issues related to **citing** those webpages should be reported to: https://eazypaper.com/support