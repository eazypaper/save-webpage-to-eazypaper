let autoClose = true;
let downloadUrl = "";
let isWindows = true;

let isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
let isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

chrome.runtime.getPlatformInfo(platformInfo => {
	isWindows = platformInfo.os == "win";
});

chrome.tabs.query({ currentWindow: true, active: true }, ([tab]) => {
	chrome.pageCapture.saveAsMHTML({ "tabId": tab.id }, (mhtmlData) => {
		chrome.downloads.setShelfEnabled(false);
		downloadUrl = URL.createObjectURL(mhtmlData.slice(0, mhtmlData.size-2, "application/x-mimearchive"));

		let title = tab.title.replace(/[\/\\:\*\?<>|~"]/gi, ' ').replace(/&nbsp;/g, " ").replace(/\s\s+/g, ' ').trim();
		download(title, (success) => {
			if (success) {
				outputSuccess(title);
				return;
			}
			title = title.substring(0, 200);
			download(title, (success) => {
				if (success) {
					outputSuccess(title);
					return;
				}
				const date = new Date();
				title = date.toISOString().replace(/:/g, "_").split(".")[0];
				document.getElementById('filename').innerHTML = title;
				download(title, (success) => { 
					if (success) {
						outputSuccess(title);
						return;
					}
				});
			});
		});
	});
});

document.addEventListener('mousemove', () => {
	autoClose = false;
}, false);

document.body.addEventListener('mouseleave', () => {
	window.close();
});

chrome.downloads.onChanged.addListener(downloadDelta => {
	URL.revokeObjectURL(downloadUrl);
	chrome.downloads.setShelfEnabled(true);
	setTimeout(() => {
		if (autoClose) {
			window.close();
		}
	}, 2000);
});

function download(title, callback) {
	chrome.downloads.download({
		url: downloadUrl,
		conflictAction: "overwrite",
		filename: "EazyPaper/" + title + ".mht",
		saveAs: isEdgeChromium,
	}, downloadId => {
		callback(chrome.runtime.lastError === undefined);
	});
}

function outputSuccess(title) {
	if (isEdgeChromium) {
		document.getElementById('path').innerHTML = "Press Enter to save, then click Cite it";
	} else {
		if (isWindows) {
			document.getElementById('path').innerHTML = "Saved to Downloads\\EazyPaper\\";
		} else {
			document.getElementById('path').innerHTML = "Saved to Downloads/EazyPaper/";
		}	
	}
	document.getElementById('wrapper').style.display = "flex";
	document.getElementById('filename').innerHTML = title;
}

document.getElementById("cite").addEventListener("click", () => {
	window.open("start.html#cite");
});